#!/bin/sh
set -e
cd hello-spring-boot
mvn clean package && cd .. && cp hello-spring-boot/target/*.jar hello-spring-boot/Dockerfile build-output
